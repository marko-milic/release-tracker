package com.neon.releasetracker.controller.v1.api;

import com.neon.releasetracker.TestUtils;
import com.neon.releasetracker.consts.APIPath;
import com.neon.releasetracker.controller.v1.model.Release;
import com.neon.releasetracker.controller.v1.model.ReleaseFilter;
import com.neon.releasetracker.controller.v1.model.Status;
import com.neon.releasetracker.service.ReleaseRepository;
import com.neon.releasetracker.service.ReleaseSpecification;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 22:42
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReleaseControllerValidMediumTest {

    private static String ENDPOINT_URL = APIPath.API + APIPath.VERSION_1 + APIPath.RELEASES;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReleaseRepository repository;

    @Test
    public void get_releases_successful() throws Exception {
        // Given
        ReleaseFilter releaseFilter = new ReleaseFilter("name", Status.IN_DEVELOPMENT, null,
                null, null);
        MultiValueMap queryParams = new LinkedMultiValueMap();
        queryParams.add("name", releaseFilter.getName());
        queryParams.add("status", releaseFilter.getStatus().toString());
        Mockito.when(repository.findAll(ArgumentMatchers.any(ReleaseSpecification.class)))
                .thenReturn(Arrays.asList(Fixture.release));

        // When
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get(ENDPOINT_URL)
                .queryParams(queryParams)
                .content(TestUtils.toJson(releaseFilter))
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .string(CoreMatchers.containsString(releaseFilter.getStatus().toString())));
    }

    static class Fixture {
        static Long id = 1L;
        static Release release = new Release(id, "name", "description", Status.IN_DEVELOPMENT,
                LocalDate.now(),
                LocalDateTime.now(), LocalDateTime.now());
        ReleaseFilter releaseFilter = new ReleaseFilter("name", Status.IN_DEVELOPMENT, LocalDate.now(),
                LocalDateTime.now(),
                LocalDateTime.now());
    }
}
