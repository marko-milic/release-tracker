package com.neon.releasetracker.controller.v1.api;

import com.neon.releasetracker.consts.APIPath;
import com.neon.releasetracker.service.ReleaseRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 24/07/2021
 * Time: 09:17
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReleaseControllerInvalidMediumTest {

    private static String ENDPOINT_URL = APIPath.API + APIPath.VERSION_1 + APIPath.RELEASES;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReleaseRepository repository;

    @Test
    public void get_releases_not_found() throws Exception {
        // When
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get(ENDPOINT_URL)
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON));

        // Then
        result.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
