package com.neon.releasetracker;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 22:50
 */
public class TestUtils {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String toJson(Object object) {
        OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        OBJECT_MAPPER.findAndRegisterModules();

        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static MockHttpServletRequestBuilder buildRequest(Object object, String url) {
        return MockMvcRequestBuilders
                .post(url)
                .content(TestUtils.toJson(object))
                .accept(MediaType.ALL)
                .contentType(MediaType.APPLICATION_JSON);
    }
}
