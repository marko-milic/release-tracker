package com.neon.releasetracker.service;

import com.neon.releasetracker.controller.v1.model.Release;
import com.neon.releasetracker.controller.v1.model.ReleaseFilter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 13:37
 */
public class ReleaseSpecification implements Specification<Release> {

    private final ReleaseFilter filter;

    public ReleaseSpecification(ReleaseFilter filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Release> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate matchName = criteriaBuilder.equal(root.get("name"), filter.getName());
        Predicate matchStatus = criteriaBuilder.equal(root.get("status"), filter.getStatus());
        Predicate matchReleaseDate = criteriaBuilder.equal(root.get("releaseDate"), filter.getReleaseDate());
        Predicate matchCreatedAt = criteriaBuilder.equal(root.get("createdAt"), filter.getCreatedAt());

        return criteriaBuilder.or(matchName, matchStatus, matchReleaseDate, matchCreatedAt);
    }
}
