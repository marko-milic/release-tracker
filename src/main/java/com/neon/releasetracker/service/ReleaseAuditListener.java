package com.neon.releasetracker.service;

import com.neon.releasetracker.controller.v1.model.Release;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 14:17
 */
public class ReleaseAuditListener {

    @PreUpdate
    private void beforeUpdate(Release release) {
        release.setLastUpdatedAt(LocalDateTime.now());
    }

    @PrePersist
    private void beforeAdd(Release release) {
        release.setCreatedAt(LocalDateTime.now());
        release.setLastUpdatedAt(LocalDateTime.now());
    }
}
