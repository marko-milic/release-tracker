package com.neon.releasetracker.controller.v1.advice;

import com.neon.releasetracker.controller.v1.api.ReleaseController;
import com.neon.releasetracker.exception.ResourceNotFoundException;
import com.neon.releasetracker.util.ResponseBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.format.DateTimeParseException;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 13:08
 */
@ControllerAdvice(assignableTypes = ReleaseController.class)
public class ReleaseAdvice {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(ResourceNotFoundException exception) {
        return ResponseBuilder.notFound();
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<?> handleDateTimeParseException(DateTimeParseException exception) {
        return ResponseBuilder.badRequest();
    }
}
