package com.neon.releasetracker.controller.v1.api;

import com.neon.releasetracker.consts.APIPath;
import com.neon.releasetracker.controller.v1.model.Release;
import com.neon.releasetracker.controller.v1.model.ReleaseFilter;
import com.neon.releasetracker.controller.v1.model.ReleaseRequest;
import com.neon.releasetracker.service.ReleaseService;
import com.neon.releasetracker.util.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:07
 */
@RestController
@RequestMapping(value = APIPath.API + APIPath.VERSION_1 + APIPath.RELEASES,
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
public class ReleaseController {

    private final ReleaseService service;

    @Autowired
    public ReleaseController(ReleaseService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<Release>> getReleases(ReleaseFilter releaseFilter) {
        List<Release> releases = service.getReleases(releaseFilter);

        return ResponseBuilder.ok(releases);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Release> getRelease(@PathVariable Long id) {
        Release release = service.getRelease(id);

        return ResponseBuilder.ok(release);
    }

    @PostMapping
    public ResponseEntity<Release> addRelease(
            @RequestBody
                    ReleaseRequest releaseRequest) {
        Release release = service.addRelease(releaseRequest);

        return ResponseBuilder.created(release);
    }

    @PutMapping
    public ResponseEntity<Release> updateRelease(
            @RequestBody
                    ReleaseRequest releaseRequest) {
        service.updateRelease(releaseRequest);

        return ResponseBuilder.ok();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Release> deleteRelease(
            @PathVariable
                    Long id) {
        service.deleteRelease(id);

        return ResponseBuilder.ok();
    }
}
