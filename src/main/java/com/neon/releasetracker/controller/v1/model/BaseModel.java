package com.neon.releasetracker.controller.v1.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:19
 */
@MappedSuperclass
public class BaseModel {
    public static final String CREATED_AT_COLUMN = "created_at";
    public static final String LAST_UPDATED_AT_COLUMN = "last_updated_at";

    @Column(name = CREATED_AT_COLUMN, nullable = false)
    private LocalDateTime createdAt;

    @Column(name = LAST_UPDATED_AT_COLUMN, nullable = false)
    private LocalDateTime lastUpdatedAt;

    public BaseModel(LocalDateTime createdAt, LocalDateTime lastUpdatedAt) {
        this.createdAt = createdAt;
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public BaseModel() {
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setLastUpdatedAt(LocalDateTime lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }
}
