package com.neon.releasetracker.consts;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 11:09
 */
public class APIPath {
    private static final String PATH_SEPARATOR = "/";

    public static final String API = PATH_SEPARATOR + "api";
    public static final String VERSION_1 = PATH_SEPARATOR + "v1";

    public static final String RELEASES = PATH_SEPARATOR + "releases";
}
