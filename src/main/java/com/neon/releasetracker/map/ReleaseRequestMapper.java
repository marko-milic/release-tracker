package com.neon.releasetracker.map;

import com.neon.releasetracker.controller.v1.model.ReleaseRequest;
import com.neon.releasetracker.controller.v1.model.Release;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 14:06
 */
public class ReleaseRequestMapper {

    public static Release map(ReleaseRequest releaseRequest) {
        return new Release(null, releaseRequest.getName(), releaseRequest.getDescription(),
                releaseRequest.getStatus(), releaseRequest.getReleaseDate(), null, null);
    }

    public static void updateReleaseFrom(Release release, ReleaseRequest releaseRequest) {
        if (releaseRequest.getReleaseDate() != null)
            release.setReleaseDate(releaseRequest.getReleaseDate());

        if (releaseRequest.getDescription() != null)
            release.setDescription(releaseRequest.getDescription());

        if (releaseRequest.getName() != null)
            release.setName(releaseRequest.getName());

        if (releaseRequest.getStatus() != null)
            release.setStatus(releaseRequest.getStatus());
    }
}
