package com.neon.releasetracker.controller.v1.advice;

import com.neon.releasetracker.exception.ResourceNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created By
 * User: Marko.Milic
 * marko_milic@outlook.com
 * Date: 23/07/2021
 * Time: 22:36
 */
public class ReleaseAdviceSmallTest {

    private ReleaseAdvice releaseAdvice;

    @Before
    public void setup() {
        releaseAdvice = new ReleaseAdvice();
    }

    @Test
    public void test_handle_entity_not_found_exception() {
        // When
        ResponseEntity<?> responseEntity = releaseAdvice.handleEntityNotFoundException(new ResourceNotFoundException());

        // Then
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }
}
